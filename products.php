<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main_heading">
                <div class="main_heading__row">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li>Products</li>
                        </ul>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">
                    <h1>vexa products</h1>

                    <div class="products_block__wrap">

                        <div class="products_block">
                            <div class="products_block__heading products_block__one">
                                <div class="products_block__icon">
                                    <img src="img/production__icon_01.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="products_block__text">
                                <h4>crypto ATM</h4>
                                <p>working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            </div>
                        </div>

                        <div class="products_block">
                            <div class="products_block__heading products_block__two">
                                <div class="products_block__icon">
                                    <img src="img/production__icon_02.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="products_block__text">
                                <h4>exchange</h4>
                                <p>working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            </div>
                        </div>

                        <div class="products_block">
                            <div class="products_block__heading products_block__three">
                                <div class="products_block__icon">
                                    <img src="img/production__icon_03.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="products_block__text">
                                <h4>token</h4>
                                <p>working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            </div>
                        </div>

                        <div class="products_block">
                            <div class="products_block__heading products_block__four">
                                <div class="products_block__icon">
                                    <img src="img/production__icon_04.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="products_block__text">
                                <h4>bot</h4>
                                <p>working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            </div>
                        </div>

                        <div class="products_block">
                            <div class="products_block__heading products_block__five">
                                <div class="products_block__icon">
                                    <img src="img/production__icon_05.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="products_block__text">
                                <h4>payment system</h4>
                                <p>working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing</p>
                            </div>
                        </div>

                    </div>
                    
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
