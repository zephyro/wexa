<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main_heading">
                <div class="main_heading__row">
                    <div class="container">
                        <div class="main_heading__wrap">
                            <div class="main_heading__col">
                                <ul class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li>About us</li>
                                </ul>
                            </div>
                            <div class="main_heading__col">
                                <ul class="main_heading__nav">
                                    <li class="active"><a href="#">2015</a></li>
                                    <li><a href="#">2016</a></li>
                                    <li><a href="#">2017</a></li>
                                    <li><a href="#">2018</a></li>
                                    <li><a href="#">2019</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">

                    <h1>company history</h1>

                    <div class="main_row">
                        <div class="main_col mb_40">
                            <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            <p>In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                        </div>
                        <div class="main_col mb_40">
                            <img src="images/img_01.jpg" class="img-fluid" alt="image">
                        </div>
                    </div>

                    <div class="main_row">
                        <div class="main_col mb_40">
                            <img src="images/img_03.jpg" class="img-fluid" alt="image">
                        </div>
                        <div class="main_col mb_40">
                            <p>Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.<br/>
                                In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            <p>Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and <a href="#">transactions opening</a> and closing.</p>
                            <p>Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            <ul class="list_styled">
                                <li>VexaGlobal platform launch</li>
                                <li>First VexaGlobal crypto ATM</li>
                                <li>Vexa cryptocurrency exchange launch</li>
                                <li>Own token release</li>
                                <li>Trading bot launch</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <section class="info info_yellow">
                <div class="info__wrap">
                    <div class="container">
                        <div class="info__row">
                            <div class="info__left pt_20">
                                <div class="info__heading">document registry</div>
                                <div class="info__text mb_40">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                                <a href="#" class="btn btn_border_dark">view registry</a>
                            </div>
                            <div class="info__right">
                                <ul class="info__doc">
                                    <li>
                                        <a href="images/doc_01.jpg" data-fancybox="doc">
                                            <img src="images/doc_thumb__01.jpg" class="img-fluid" alt="doc">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="images/doc_02.jpg" data-fancybox="doc">
                                            <img src="images/doc_thumb__02.jpg" class="img-fluid" alt="doc">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="contact">
                <div class="container">
                    <div class="contact__row">
                        <div class="contact__info">
                            <h4>contacts</h4>
                            <ul>
                                <li><strong>E-mail: </strong> <a href="#">vexaglobal@gmail.com</a></li>
                                <li><strong>Skype: </strong> <a href="skype:VexaGlobal">VexaGlobal</a></li>
                                <li><strong>Phone: </strong> <a href="tel:+38 (096) 384-84-44">+38 (096) 384-84-44</a></li>
                                <li><strong>Telegram: </strong> <a href="#">@Vexa</a></li>
                            </ul>
                        </div>
                        <div class="contact__form">
                            <div class="contact__form_heading">
                                <h4>feedback</h4>
                            </div>
                            <form class="form">
                                <div class="form_row">
                                    <div class="form_col_elem">
                                        <div class="form_group">
                                            <label class="form_label">Category:</label>
                                            <select class="form_control form_select">
                                                <option value="Help & Support">Help & Support</option>
                                                <option value="Help & Support">Help & Support</option>
                                                <option value="Help & Support">Help & Support</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form_col_elem">
                                        <div class="form_group">
                                            <label class="form_label">Your e-mail:</label>
                                            <div class="form_item">
                                                <input class="form_control" type="text" name="email" placeholder="dkarts2009@gmail.com">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_group">
                                    <label class="form_label">Message title:</label>
                                    <div class="form_item">
                                        <input class="form_control" type="text" name="message" placeholder="">
                                    </div>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Message text:</label>
                                    <div class="form_item">
                                        <textarea class="form_control" name="message" placeholder="" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="form_row">
                                    <div class="form_col center_box">
                                        <img src="images/recaptcha.png" class="img-fluid" alt="recaptcha">
                                    </div>
                                    <div class="form_col center_box">
                                        <button type="submit" class="btn btn_yellow">send</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
