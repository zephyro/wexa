<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main_heading">
                <div class="main_heading__row">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Last news</a></li>
                            <li>How XRP compares to rival cryptocurrencies</li>
                        </ul>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">
                    <h1>How XRP compares to rival cryptocurrencies</h1>
                    <div class="main_row">
                        <div class="main_col">
                            <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                            <p>In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader </p>
                        </div>
                        <div class="main_col mb_20">
                            <img src="images/img_02.jpg" class="img-fluid" alt="image">
                        </div>
                    </div>
                    <p>working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. <a href="#">Trading Room</a> you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>
                    <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</p>

                    <div class="mb_40"></div>

                    <div class="heading_line"><span>more news</span></div>

                    <ul class="news__row">
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">19 March 2019</div>
                                    <div class="news_item__title">Live trading room</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">08 March 2019</div>
                                    <div class="news_item__title">Money Regulations 2019</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">28 Feb 2019</div>
                                    <div class="news_item__title">How XRP compares to rival cryptocurrencies </div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">17 Feb 2019</div>
                                    <div class="news_item__title">How to make money?</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
