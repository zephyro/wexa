<!-- Auth modal -->
<div class="hide">
    <a href="#auth" class="auth_open btn_modal"></a>
    <div class="modal tabs" id="auth">
        <ul class="modal__nav tabs_nav">
            <li class="tab_reg active"><a href="#tab_reg"><span class="hide-md"><i class="far fa-user"></i></span><span class="hide-xs-only hide-sm-only">register</span></a></li>
            <li class="tab_auth"><a href="#tab_auth"><span class="hide-md"><i class="fas fa-sign-in-alt"></i></span><span class="hide-xs-only hide-sm-only">sign in</span></a></li>
            <li class="tab_restore"><a href="#tab_restore"><span class="hide-md"><i class="fas fa-key"></i></span><span class="hide-xs-only hide-sm-only">restore pass</span></a></li>
        </ul>
        <div class="modal__body">

            <div class="tabs_item active" id="tab_reg">
                <form class="form">
                    <div class="form_group">
                        <label class="form_label">E-mail:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="email" placeholder="mail@gmail.com">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <label class="form_label">Password:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="pass" placeholder="********">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <label class="form_label">Repeat password:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="pass2" placeholder="********">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <div class="form_captcha">
                            <img src="images/recaptcha.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="form_col center_box">
                            <label class="form_checkbox">
                                <input type="checkbox" name="check">
                                <span>Accept <a href="#">User agreement</a></span>
                            </label>
                        </div>
                        <div class="form_col">
                            <button type="submit" class="btn btn_yellow">register</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="tabs_item" id="tab_auth">
                <form class="form">
                    <div class="form_group">
                        <label class="form_label">E-mail:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="email" placeholder="mail@gmail.com">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <label class="form_label">Password:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="pass" placeholder="********">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <div class="form_captcha">
                            <img src="images/recaptcha.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="form_col center_box">
                            <a href="#">Forgot your Password</a>
                        </div>
                        <div class="form_col">
                            <button type="submit" class="btn btn_yellow">register</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="tabs_item" id="tab_restore">
                <form class="form">
                    <div class="form_group">
                        <label class="form_label">E-mail:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="email" placeholder="mail@gmail.com">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <label class="form_label">Password:</label>
                        <div class="form_item">
                            <input type="text" class="form_control" name="pass" placeholder="********">
                            <span class="form_message"></span>
                        </div>
                    </div>
                    <div class="form_group">
                        <div class="form_captcha">
                            <img src="images/recaptcha.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="form_col center_box">
                            <a href="#">Forgot your Password</a>
                        </div>
                        <div class="form_col">
                            <button type="submit" class="btn btn_yellow">register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- -->

<!-- Auth modal -->
<div class="hide">
    <a href="#confirm" class="confirm_open btn_modal"></a>
    <div class="modal" id="confirm">
        <div class="modal__heading">Confirm register</div>
        <div class="modal__confirm">You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader</div>
        <form class="form">
            <div class="form_group mb_40">
                <label class="form_label">Code ID:</label>
                <div class="form_item">
                    <input type="text" class="form_control" name="confirm" placeholder="" value="x388jdglla9122ddw">
                    <span class="form_message"></span>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn_yellow">confirm</button>
            </div>
        </form>
    </div>
</div>
<!-- -->