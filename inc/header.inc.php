<div class="top">
    <div class="container">
        <div class="top__row">
            <ul class="top__nav">
                <li><a href="#">Privacy Notice</a></li>
                <li><a href="#">help & support</a></li>
                <li><a href="#">Cookies Notice</a></li>
            </ul>
            <div class="top__lng">
                <div class="lng">
                    <div class="lng__active">
                        <i class="fas fa-angle-down"></i>
                        <span>english</span>
                        <b><img src="img/lng__en.png" class="img-fluid" alt=""></b>
                    </div>
                    <ul class="lng__dropdown">
                        <li>
                            <a href="#">
                                <span>english</span>
                                <i><img src="img/lng__en.png" class="img-fluid" alt=""></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>русский</span>
                                <i><img src="img/lng__ru.png" class="img-fluid" alt=""></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>deutsch</span>
                                <i><img src="img/lng__de.png" class="img-fluid" alt=""></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>poland</span>
                                <i><img src="img/lng__pl.png" class="img-fluid" alt=""></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__logo" href="#">
                <img src="img/header__logo.png" class="img-fluid" alt="">
            </a>

            <a class="header__toggle nav_toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>

            <nav class="header__menu">

                <ul class="header__nav">
                    <li class="active"><a href="#"><span>Home</span></a></li>
                    <li><a href="#"><span>How to start</span></a></li>
                    <li><a href="#"><span>News</span></a></li>
                    <li><a href="#"><span>Contacts</span></a></li>
                    <li><a href="#"><span>About us</span></a></li>
                </ul>

                <ul class="header__second">
                    <li><a href="#">Privacy Notice</a></li>
                    <li><a href="#">help & support</a></li>
                    <li><a href="#">Cookies Notice</a></li>
                </ul>

                <ul class="header__buttons">
                    <li><a href="#" data-target="tab_reg" class="btn btn_auth">register</a></li>
                    <li><a href="#" data-target="tab_auth" class="btn btn_yellow btn_auth">Sign In</a></li>
                </ul>

            </nav>
        </div>
    </div>
</header>