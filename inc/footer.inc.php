<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__left">
                <a href="#" class="footer__logo">
                    <img src="img/header__logo.png" class="img-fluid" alt="">
                </a>
                <div>
                    © 2019 VexaGlobal.com<br/>
                    All rights reserved.
                </div>
            </div>
            <div class="footer__center">
                <ul class="footer__nav">
                    <li>
                        <a href="#">
                            <i class="fas fa-angle-right"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-angle-right"></i>
                            <span>Contacts</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-angle-right"></i>
                            <span>How to start</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-angle-right"></i>
                            <span>About us</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-angle-right"></i>
                            <span>News</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer__right">
                <div class="footer__social">
                    <a href="#"><i class="fab fa-vk"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>