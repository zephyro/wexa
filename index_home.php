<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="promo">
                <div class="container">
                    <div class="promo__row">

                        <div class="promo__content">
                            <div class="promo__title">
                                <strong>vexa global inc</strong>
                                <span>we are the Future!</span>
                            </div>
                            <div class="promo__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                            <ul class="promo__buttons">
                                <li>
                                    <a href="#" class="btn btn_white btn_auth" data-target="tab_reg">register</a>
                                </li>
                                <li>
                                    <a href="#confirm" class="btn btn_border_yellow btn_modal">try now</a>
                                </li>
                            </ul>
                        </div>

                        <div class="promo__image">
                            <div class="promo__image_item">
                                <img src="img/promo__image.png" class="img-fluid" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="info info_yellow">
                <div class="info__wrap">
                    <div class="container">
                        <div class="info__row">
                            <div class="info__left">
                                <div class="info__heading">Company History</div>
                                <div class="info__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</div>
                            </div>
                            <div class="info__right">
                                <div class="info__image">
                                    <div class="info__image_item">
                                        <img src="img/history__image.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="advantage">
                <div class="container">
                    <div class="heading_line"><span>our advantages</span></div>
                    <ul>
                        <li>
                            <h4>corporation</h4>
                            <p>You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. We are a team of technology and finance hotheads</p>
                        </li>
                        <li>
                            <h4>contracts and documents</h4>
                            <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. </p>
                        </li>
                        <li>
                            <h4>Technical support</h4>
                            <p>You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset.</p>
                        </li>
                        <li>
                            <h4>simple platform</h4>
                            <p>You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset.</p>
                        </li>
                        <li>
                            <h4>investments</h4>
                            <p>You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset.</p>
                        </li>
                        <li>
                            <h4>smart systems</h4>
                            <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</p>
                        </li>
                    </ul>
                </div>
            </section>

            <section class="production">
                <div class="container">
                    <div class="heading">our products</div>
                    <ul class="production__row">

                        <li>
                            <div class="production_item">
                                <div class="production_item__icon">
                                    <img src="img/production__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <h4>crypto ATM</h4>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. Trading Room you can access the screen of a trader working for Exp Asset.</p>
                            </div>
                        </li>

                        <li>
                            <div class="production_item">
                                <div class="production_item__icon">
                                    <img src="img/production__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <h4>exchange</h4>
                                <p>We believe they will become money of the 21st century. Trading Room you can access the screen of a trader working for Exp Asset. We are a team of technology and finance hotheads.</p>
                            </div>
                        </li>

                        <li>
                            <div class="production_item">
                                <div class="production_item__icon">
                                    <img src="img/production__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <h4>token</h4>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</p>
                            </div>
                        </li>

                        <li>
                            <div class="production_item">
                                <div class="production_item__icon">
                                    <img src="img/production__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <h4>bot</h4>
                                <p>We believe they will become money of the 21st century. Trading Room you can access the screen of a trader working for Exp Asset. We are a team of technology and finance hotheads.</p>
                            </div>
                        </li>

                        <li>
                            <div class="production_item">
                                <div class="production_item__icon">
                                    <img src="img/production__icon_05.png" class="img-fluid" alt="">
                                </div>
                                <h4>payment system</h4>
                                <p>We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. Trading Room you can access the screen of a trader working for Exp Asset.</p>
                            </div>
                        </li>

                    </ul>
                </div>
            </section>

            <section class="roadmap">
                <div class="container">
                    <div class="heading">our roadmap</div>

                    <div class="roadmap__wrap">
                        <div class="roadmap__scale">
                            <div class="roadmap__scale_progress" style="height: 160px"></div>
                        </div>
                        <ul class="roadmap__line">
                            <li class="successfully">
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_item_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>aPRIL 2019</strong>
                                        <span>VexaGlobal platform launch</span>
                                    </div>
                                </div>
                            </li>
                            <li class="successfully">
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>may 2019</strong>
                                        <span>First VexaGlobal crypto ATM</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_item_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>JUly 2019</strong>
                                        <span>Vexa cryptocurrency exchange launch</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>september 2019</strong>
                                        <span>Own token release</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_item_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>november 2019</strong>
                                        <span>Trading bot launch</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="roadmap__point"></span>
                                <div class="roadmap_item roadmap_invert">
                                    <div class="roadmap_item__icon"><i></i></div>
                                    <div class="roadmap_item__text">
                                        <strong>January 2020</strong>
                                        <span>Vexa payment system launch</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="roadmap__logo">
                        <img src="img/roadmap__logo.jpg" class="img-fluid" alt="">
                    </div>

                </div>
            </section>

            <section class="info info_dark">
                <div class="info__wrap">
                    <div class="container">
                        <div class="info__row">
                            <div class="info__left">
                                <div class="info__heading">crypto atm</div>
                                <div class="info__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century. In the Live Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing. You will see his analysis and transactions opening and closing. Trading Room you can access the screen of a trader working for Exp Asset. You will see his analysis and transactions opening and closing.</div>
                            </div>
                            <div class="info__right">
                                <div class="info__image">
                                    <div class="info__image_item">
                                        <img src="img/crypto.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="news">
                <div class="container">
                    <div class="heading_line"><span>last news</span></div>
                    <ul class="news__row">
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">19 March 2019</div>
                                    <div class="news_item__title">Live trading room</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">08 March 2019</div>
                                    <div class="news_item__title">Money Regulations 2019</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">28 Feb 2019</div>
                                    <div class="news_item__title">How XRP compares to rival cryptocurrencies </div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="news_item" href="#">
                                <div class="news_item__image">
                                    <img src="images/news__04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="news_item__text">
                                    <div class="news_item__date">17 Feb 2019</div>
                                    <div class="news_item__title">How to make money?</div>
                                </div>
                                <div class="news_item__arrow">
                                    <i class="fas fa-angle-right"></i>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <div class="text-center">
                        <a href="#" class="news__load">
                            <span>More news</span>
                            <i class="fas fa-angle-down"></i>
                        </a>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
