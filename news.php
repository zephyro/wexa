<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main_heading">
                <div class="main_heading__row">
                    <div class="container">
                        <div class="main_heading__wrap">
                            <div class="main_heading__col">
                                <ul class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li>Last news</li>
                                </ul>
                            </div>
                            <div class="main_heading__col">
                                <ul class="main_heading__nav">
                                    <li class="active"><a href="#">2015</a></li>
                                    <li><a href="#">2016</a></li>
                                    <li><a href="#">2017</a></li>
                                    <li><a href="#">2018</a></li>
                                    <li><a href="#">2019</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="main">
                <div class="container">

                    <h1>last news</h1>

                    <div class="intro_row">

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_01.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">Live trading room</a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_02.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">How XRP compares to rival cryptocurrencies </a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. </div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_03.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">Money Regulations 2019</a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_04.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">How to make money?</a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_02.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">Live trading room</a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. We believe they will become money of the 21st century.</div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="intro_wrap">
                            <article class="intro">
                                <a href="#" class="intro__image">
                                    <img src="images/news_01.jpg" class="img-fluid" alt="news">
                                </a>
                                <div class="intro__content">
                                    <div class="intro__date">19 March 2019</div>
                                    <h5><a href="#">How XRP compares to rival cryptocurrencies </a></h5>
                                    <div class="intro__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies and their role in changing world. </div>
                                    <div class="intro__bottom">
                                        <a href="#" class="btn">read more</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>

                    <div class="text-center">
                        <a href="#" class="news__load">
                            <span>More news</span>
                            <i class="fas fa-angle-down"></i>
                        </a>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
